package service;

import gui.Controller;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.util.concurrent.TimeUnit;


public class LoopService extends Service<Void> {

    private boolean buttonStatus = false;

    private int timeInSeconds;

    public LoopService(int timeInSeconds) {
        this.timeInSeconds = timeInSeconds;
    }

    @Override
    protected Task<Void> createTask() {
        return new MyTask(this);
    }

    public boolean getButtonStatus() {
        return buttonStatus;
    }

    public void setButtonStatus(boolean buttonStatus) {
        this.buttonStatus = buttonStatus;
    }

    public int getTimeInSeconds() {
        return timeInSeconds;
    }

    public void setTimeInSeconds(int timeInSeconds) {
        this.timeInSeconds = timeInSeconds;
    }

}

class MyTask extends Task<Void> {

    LoopService service;

    public MyTask(LoopService service) {
        this.service = service;
    }


    @Override
    protected Void call() throws Exception {
        while (service.getButtonStatus()) {
            Controller.playSound(gui.Controller.path);
            TimeUnit.SECONDS.sleep(service.getTimeInSeconds());
            //System.out.println("Loop was checked");
        }

        //System.out.println("Finished");
        return null;
    }
}
